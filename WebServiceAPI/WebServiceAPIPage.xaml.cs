﻿using Xamarin.Forms;
using WebServiceAPI.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Plugin.Connectivity;

namespace WebServiceAPI
{
    public partial class WebServiceAPIPage : ContentPage
    {
        public WebServiceAPIPage()
        {          
                InitializeComponent();
        }

        async void ClickedOnEnter(object sender, System.EventArgs e)
        {
            string word1 = word_entry.Text;
            HttpClient client = new HttpClient();

            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v2/dictionary/{word1}"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);
            DictionaryItem[] dictionaryAPIData = null;
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                dictionaryAPIData = DictionaryItem.FromJson(content);
                TypeData.Text = $"Type is {dictionaryAPIData[0].Type}";
                DefinitionData.Text = $"Definition is {dictionaryAPIData[0].Definition}";
                ExampleData.Text = $"Example is {dictionaryAPIData[0].Example}";

            }

        }
    }
}
