﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebServiceAPI.Models
{
    public partial class DictionaryItem
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class Type
    {
        [JsonProperty("noun")]
        public string Noun { get; set; }

        [JsonProperty("verb")]
        public string Verb { get; set; }

        [JsonProperty("adjective")]
        public string Adjective { get; set; }

        [JsonProperty("adverb")]
        public string Adverb { get; set; }

        [JsonProperty("pronoun")]
        public string Pronoun { get; set; }

        [JsonProperty("preposition")]
        public string Preposition { get; set; }

        [JsonProperty("conjunction")]
        public string Conjunction { get; set; }

        [JsonProperty("determiner")]
        public string Determiner { get; set; }

        [JsonProperty("contraction")]
        public string Contraction { get; set; }
    }

    public partial class DictionaryItem
    {
        public static DictionaryItem[] FromJson(string json) =>
        JsonConvert.DeserializeObject<DictionaryItem[]>(json, WebServiceAPI.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DictionaryItem[] self) =>
        JsonConvert.SerializeObject(self, WebServiceAPI.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            },
        };
    }
}

