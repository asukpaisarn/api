﻿using Xamarin.Forms;
using Plugin.Connectivity;

namespace WebServiceAPI
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingCenter.Send<WebServiceAPIPage>(null, "Internet Not Connected");
                }
            };
            MainPage = new WebServiceAPIPage();
        }

        protected override void OnStart()
        {
             //Handle when your app starts
            //CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            //{
            //    if (!CrossConnectivity.Current.IsConnected)
            //    {
            //        MessagingCenter.Send<WebServiceAPIPage>(null, "Internet Not Connected");
            //    }
            //};
           
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
